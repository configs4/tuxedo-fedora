# setup Obsidian

[source](https://viniciusnevescosta.medium.com/syncing-and-backing-up-your-thoughts-with-obsidian-syncthing-and-gitwatch-a55670b2b63f)

# syncing

install syncthing

```bash
sudo dnf install syncthing
```

auto start syncthing

```bash
#create systemd dir
cd /
mkdir -p "$HOME/.config/systemd/user && cd $HOME/.config/systemd/user"
```

create file syncthing@.service

```bash
[Unit]
Description=Syncthing - Open Source Continuous File Synchronization for %I
Documentation=man:syncthing(1)
After=network.target
StartLimitIntervalSec=60
StartLimitBurst=4

[Service]
User=%i
ExecStart=/usr/bin/syncthing serve --no-browser --no-restart --logflags=0
Restart=on-failure
RestartSec=1
SuccessExitStatus=3 4
RestartForceExitStatus=3 4

# Hardening
ProtectSystem=full
PrivateTmp=true
SystemCallArchitectures=native
MemoryDenyWriteExecute=true
NoNewPrivileges=true

# Elevated permissions to sync ownership (disabled by default),
# see https://docs.syncthing.net/advanced/folder-sync-ownership
#AmbientCapabilities=CAP_CHOWN CAP_FOWNER

[Install]
WantedBy=multi-user.target
```

start service

```bash
systemctl enable syncthing@bvd.service
systemctl start syncthing@bvd.service
```

## backup

install GitWatch

```bash
sudo dnf install git inotify-tools
git clone https://github.com/gitwatch/gitwatch.git
cd gitwatch
sudo install -b gitwatch.sh /usr/local/bin/gitwatch
```

setup gitwatch

```bash
cd ~/Documents/obsidianVaults
git init
```

tryout gitwatch

```bash
cd /
gitwatch -s 1 -r git@gitlab.com:obsidian-bvd/backup.git -R -b main ~/Documents/obsidianVaults
```

Automate gitwatch

```bash
#create systemd dir
cd /
mkdir -p "$HOME/.config/systemd/user && cd $HOME/.config/systemd/user"
```

create file gitwatch@obsidian.service

```bash
[Unit]
Description=Watch Obsidian dir and backup to git

[Service]
ExecStart=/usr/local/bin/gitwatch -s 10 -r git@gitlab.com:obsidian-bvd/backup.git -R -b main /home/bvd/Documents/obsidianVaults

[Install]
WantedBy=default.target
```

start service

```bash
systemctl --user enable gitwatch@obsidian.service
systemctl --user start gitwatch@obsidian.service
```
