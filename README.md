# Tuxedo-Fedora

This project discribes the setup for the fedora on a tuxedo laptop

## install distro

install fedora from usb

## update

```bash
sudo dnf update
sudo dnf upgrade
sudo reboot
```

## gen ssh keys

```bash
ssh-keygen
```

place the keys in gitlab

## install tuxedo software

```bash
dnf copr enable kallepm/tuxedo-keyboard
dnf install tuxedo-keyboard
dnf copr enable kallepm/tuxedo-control-center
dnf install tuxedo-control-center
```

## enable RPM Fusion

[source](https://docs.fedoraproject.org/es/quick-docs/setup_rpmfusion/)

```bash
sudo dnf install \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

sudo dnf install \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf update
sudo dnf upgrade
```

## faster download packages

```bash
sudo dnf install vim
sudo vim /etc/dnf/dnf.conf
```

append the following

```bash
max_parallel_downloads=10
fastestmirror=true
deltarpm=true
```

## install multimedia plugin

```bash
sudo dnf install vlc

sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel

sudo dnf install lame\* --exclude=lame-devel

sudo dnf group upgrade --with-optional Multimedia
```

## flatpak repo enable

```bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

## scaling

If you’re running wayland you can use:

```bash
gsettings set org.gnome.mutter experimental-features "['scale-monitor-framebuffer']"
```

to remove:

```bash
gsettings reset org.gnome.mutter experimental-features
```

https://wiki.archlinux.org/title/HiDPI#GNOME

## NIX

[source](https://github.com/DeterminateSystems/nix-installer)

```bash
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
```

## development environment

### nerd font

```bash
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/3270.zip -O /tmp/3270.zip
mkdir -p ~/.local/share/fonts/3270
unzip /tmp/3270.zip -d ~/.local/share/fonts/3270/
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/Hack.zip -O /tmp/hack.zip
mkdir -p ~/.local/share/fonts/hack
unzip /tmp/hack.zip -d ~/.local/share/fonts/hack/
fc-cache -f -v
fc-list | grep 3270
fc-list | grep hack
```

### dotfiles

```bash
git clone https://gitlab.com/configs4/dotfiles .config/dotfiles
cd .config/dotfiles
bash bash symlink.sh
cd ~/.config/dotfiles/terminal/kitty-gruvbox-theme
git clone  https://github.com/wdomitrz/kitty-gruvbox-theme .
```

### terminal

```bash
sudo dnf copr enable atim/starship
sudo dnf install fish kitty starship
kitty +list-fonts
which fish
cat /etc/shells
chsh -s /bin/fish
```

[add custom keyboard shortcut](https://docs.fedoraproject.org/en-US/quick-docs/gnome-setting-key-shortcut/)

```
name: Kitty launcher
Command: kitty
shortcut: Alt + Super + T
```

### lazygit

```bash
sudo dnf copr enable atim/lazygit -y
sudo dnf install lazygit
```

### docker

[source](https://docs.docker.com/engine/install/fedora/#install-using-the-repository)

```bash
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo

sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo systemctl start docker
sudo systemctl enable docker

sudo groupadd docker
sudo usermod -aG docker $USER
```

logout and back in

### kubernetes

[source](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

```bash
curl -LO https://dl.k8s.io/release/v1.28.3/bin/linux/amd64/kubectl
curl -LO "https://dl.k8s.io/release/v1.28.3/bin/linux/amd64/kubectl.sha256"
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check

chmod +x kubectl
mkdir -p ~/.local/bin
mv ./kubectl ~/.local/bin/kubectl

kubectl version --client --output=yaml
```

### pyenv

[pyenv-installer](https://github.com/pyenv/pyenv-installer)
[pyenv-pyright](https://github.com/alefpereira/pyenv-pyright)

```bash
curl https://pyenv.run | bash
echo $(pyenv root)
git clone https://github.com/alefpereira/pyenv-pyright.git $(pyenv root)/plugins/pyenv-pyright
```

setup global python

```bash
sudo dnf install \
    bzip2-devel \
    libffi-devel \
    sqlite-devel \
    openssl \
    xz-devel \
    tk-devel \
    readline-devel
pyenv install 3.11.4
pyenv virtualenv 3.11.4 pyglobal-3-11-4
pyenv global pyglobal-3-11-4
```

### nvim

```bash
pyenv virtualenv 3.11.4 py-nvim-3-11
sudo dnf install \
    nodejs \
    golang \
    rust cargo \
    lua luarocks \
    clang \
    fzf \
    cmake \
    fd-find \
    neovim
npm install -g neovim
git clone git@gitlab.com:configs4/nvim.git ~/.config/nvim
cd .config/nvim
pip install -r requirements.txt
nvim
```

run `:checkheatlht` inside of neovim

## software

### flatpak

```bash
flatpak install \
    bitwarden \
    flatseal \
    spotify \
    slack \
    calibre \
    gittyup \
    VSCodium \
    Zoom \
    transmission \
    Extensionmanager \
    org.gnome.Extensions \
    discord \
    authy \
    dbeaver \
    inkscape \
    obsidian \
    onlyoffice \
    signal

flatpak install \
    signal
```

### rpm install

```bash
sudo dnf install \
    tor \
    gnome-tweaks \
    btrfs-assistant \
    cpu-x
```

## gnome

open "Extension Manager"
and install the following

- Bluetooth Quick Connect
- Caffeine
- GSConnect
- Pano - Clipboard Manager
- Vitals
