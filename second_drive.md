# Setup second drive for home

## Format application drive

This is a guide how to format the application drive where we store all the docker application data

### inspect

See all the datablock on the server

```
lsblk
```

See all the disk connected to the server

```
fdisk -l
```

### create partition table

watch https://www.youtube.com/watch?v=2Z6ouBYfZr8

```
fdisk /dev/<disk name>
# see help: m
# print the partisions: p   print the partition table
# create a partition label with : g create a new empty GPT partition table
# add a new partition: n   add a new partition
```

### format the partition

watch https://www.youtube.com/watch?v=2Z6ouBYfZr8

```
mkfs.btrfs -L application-drive /dev/partition
# example mkfs.btrfs -L application-drive /dev/sde1
```

## prepare BTRFS drive for home files

video: https://www.youtube.com/watch?v=RPO-fS6HQbY

### mount drive

```
mkdir /mnt/personal-files
mount /dev/<sd device> /mnt/personal-files
df -h
```

## create subvolumes

```
btrfs subvolume create /mnt/personal-files/documents
btrfs subvolume create /mnt/personal-files/projects
btrfs subvolume create /mnt/personal-files/wb
btrfs subvolume create /mnt/personal-files/pictures
btrfs subvolume create /mnt/personal-files/videos
btrfs subvolume create /mnt/personal-files/ssh
btrfs subvolume list /mnt/personal-files
btrfs subvolume show /mnt/personal-files
```

## mount subvolumes

find uuid of drive

```
ls -alt /dev/disk/by-uuid/
# or
blkid /dev/sda1
```

backup fstab

```
cp /etc/fstab /etc/fstab.backup
```

append lines to subvolume

```
UUID=<uuid> /home/bvd/Projects      btrfs   subvol=projects,compress=zstd:1 0 0
UUID=<uuid> /home/bvd/WB            btrfs   subvol=wb,compress=zstd:1 0 0
UUID=<uuid> /home/bvd/Documents     btrfs   subvol=documents,compress=zstd:1 0 0
UUID=<uuid> /home/bvd/Pictures      btrfs   subvol=pictures,compress=zstd:1 0 0
UUID=<uuid> /home/bvd/Videos        btrfs   subvol=videos,compress=zstd:1 0 0
UUID=<uuid> /home/bvd/.ssh          btrfs   subvol=ssh,compress=zstd:1 0 0
```
